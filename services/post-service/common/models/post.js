'use strict';

module.exports = function(Post) {
  Post.observe('before save', async (ctx) => {
    const {
      Author,
    } = Post.app.models;
    const {
      accessToken,
    } = ctx.options;

    const [author] = await Author.findOrCreate({
      where: {
        reference: accessToken.userId,
      },
    }, {
      firstname: accessToken.firstname,
      lastname: accessToken.lastname,
      reference: accessToken.userId,
    });
    ctx.instance.authorId = author.id;
    return Promise.resolve();
  });
};
